package org.lgh.sbk.ui.admin;

import android.widget.Filter;

import org.lgh.sbk.entity.Professor;
import org.lgh.sbk.entity.Student;

import java.util.List;
import java.util.stream.Collectors;

public class CustomProfessorFilter extends Filter {
    List<Professor> filterList;
    ProfessorAdapter adapter;

    public CustomProfessorFilter(List<Professor> filterList, ProfessorAdapter adapter) {
        this.filterList = filterList;
        this.adapter = adapter;
    }

    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults results = new FilterResults();
        results.count = filterList.size();
        results.values = filterList;

        if (constraint != null && constraint.length() > 0) {
            List<Professor> filterModels = filterList.stream()
                    .filter(model -> model.getName().toUpperCase().contains(constraint.toString().toUpperCase()))
                    .collect(Collectors.toList());
            results.count = filterModels.size();
            results.values = filterModels;
        }

        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {
        adapter.models = (List<Professor>) results.values;
        adapter.notifyDataSetChanged();
    }
}
