package org.lgh.sbk.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ListView;

import org.lgh.sbk.R;
import org.lgh.sbk.adapter.ScheduleWeeklyDetailAdapter;
import org.lgh.sbk.entity.ScheduleCustom;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WeeklyDetailsActivity extends AppCompatActivity {

    @BindView(R.id.list_view_details)
    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weekly_details);
        ButterKnife.bind(this);
        Intent intent = getIntent();
        ScheduleCustom scheduleCustom = (ScheduleCustom) intent.getSerializableExtra("scheduleDetail");
        ScheduleWeeklyDetailAdapter adapter = new ScheduleWeeklyDetailAdapter(WeeklyDetailsActivity.this, scheduleCustom.getScheduleList());
        listView.setAdapter(adapter);
    }
}
