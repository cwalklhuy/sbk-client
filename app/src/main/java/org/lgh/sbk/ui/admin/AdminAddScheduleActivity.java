package org.lgh.sbk.ui.admin;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.reflect.TypeToken;

import org.lgh.sbk.R;
import org.lgh.sbk.api.JsonPlaceHolderApi;
import org.lgh.sbk.api.common.ResponseModel;
import org.lgh.sbk.api.common.RetrofitClientInstance;
import org.lgh.sbk.entity.RegisterSchedule;
import org.lgh.sbk.entity.Section;
import org.lgh.sbk.entity.json.GetISectionListDeserializer;
import org.lgh.sbk.ui.AdminActivity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AdminAddScheduleActivity extends AppCompatActivity {

    @BindView(R.id.planets_spinner)
    Spinner spinner;
    @BindView(R.id.studentId)
    TextView studentId;
    @BindView(R.id.dayStudy)
    TextView dayStudy;
    @BindView(R.id.room)
    TextView room;
    @BindView(R.id.slot)
    TextView slot;
    String sectionNo = "";
    List<String> listSectionNo = new ArrayList<>();
    ArrayAdapter<String> arrayAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_add_schedule);

        ButterKnife.bind(this);
        initSection();

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                sectionNo = (String) parent.getItemAtPosition(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }



    private List<String> parseListDay(String day) {
        int year = Calendar.getInstance().get(Calendar.YEAR);
        Pattern pattern = Pattern.compile(", ");
        return pattern.splitAsStream(day)
                .map(e -> String.valueOf(year) + "-" + e)
                .collect(Collectors.toList());
    }

    private List<String> parseListStudent(String student) {
        return Pattern.compile(", ")
                .splitAsStream(student)
                .collect(Collectors.toList());
    }

    private List<String> parseListSlot(String slot) {
        return Pattern.compile(", ")
                .splitAsStream("slot " + slot)
                .collect(Collectors.toList());
    }

    @OnClick(R.id.btnCreateSchedule)
    public void createSchedule(View view) {
        RegisterSchedule registerSchedule = RegisterSchedule.builder()
                .dates(parseListDay(dayStudy.getText().toString()))
                .room(room.getText().toString())
                .sectionNo(sectionNo)
                .slotName(slot.getText().toString())
                .studentNumbers(parseListStudent(studentId.getText().toString()))
                .build();
        RetrofitClientInstance
                .getRetrofitInstance()
                .create(JsonPlaceHolderApi.class)
                .makeSchedule(registerSchedule)
                .enqueue(new Callback<ResponseModel>() {
                    @Override
                    public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                        if (!response.isSuccessful()) {
                            Log.e("FAIL", "Unsuccessfull: " + response.code());
                        }
                        ResponseModel responseModel = response.body();
                        if (responseModel.getState().equals("SUCCESS")) {
                            Toast.makeText(AdminAddScheduleActivity.this, responseModel.getMsgId(), Toast.LENGTH_LONG).show();
                            Intent intent = new Intent(AdminAddScheduleActivity.this, AdminActivity.class);
                        } else {
                            Toast.makeText(AdminAddScheduleActivity.this, responseModel.getMsgId(), Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseModel> call, Throwable t) {

                    }
                });


//        System.out.println(parseListDay(dayStudy.getText().toString()));
//        System.out.println(parseListStudent(studentId.getText().toString()));
//        System.out.println(parseListSlot(slot.getText().toString()));
    }


    private void initSection() {
        RetrofitClientInstance
                .getRetrofitInstance(new TypeToken<List<Section>>(){}.getType(), new GetISectionListDeserializer())
                .create(JsonPlaceHolderApi.class)
                .getSections()
                .enqueue(new Callback<List<Section>>() {
                    @Override
                    public void onResponse(Call<List<Section>> call, Response<List<Section>> response) {
                        if (!response.isSuccessful()) {
                            Log.e("FAIL", "Unsuccessfull: " + response.code());
                        }
                        List<Section> sections = response.body();
                        listSectionNo.addAll(sections.stream()
                                .map(Section::getSectionNo)
                                .collect(Collectors.toList()));
                        arrayAdapter = new ArrayAdapter<>(AdminAddScheduleActivity.this, android.R.layout.simple_spinner_item, listSectionNo);
                        spinner.setAdapter(arrayAdapter);
                    }

                    @Override
                    public void onFailure(Call<List<Section>> call, Throwable t) {
                        Log.e("onFailure", t.getMessage());
                    }
                });
    }
}
