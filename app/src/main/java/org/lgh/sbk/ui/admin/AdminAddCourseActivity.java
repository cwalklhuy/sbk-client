package org.lgh.sbk.ui.admin;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import org.lgh.sbk.R;
import org.lgh.sbk.api.JsonPlaceHolderApi;
import org.lgh.sbk.api.common.ResponseModel;
import org.lgh.sbk.api.common.RetrofitClientInstance;
import org.lgh.sbk.entity.Course;
import org.lgh.sbk.ui.AdminActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AdminAddCourseActivity extends AppCompatActivity {

    @BindView(R.id.courseCode)
    EditText courseCode;
    @BindView(R.id.courseName)
    EditText courseName;
    @BindView(R.id.credit)
    EditText credit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_add_course);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.btnCreateCourse)
    public void addCourse(View view) {

        Course course = Course.builder()
                .courseCode(courseCode.getText().toString())
                .courseName(courseName.getText().toString())
                .credits(Integer.valueOf(credit.getText().toString()))
                .build();

        ProgressDialog dialog = ProgressDialog.show(this, "Process...", "Please wait...", true);
        dialog.setMax(100);
        dialog.show();

        RetrofitClientInstance
                .getRetrofitInstance()
                .create(JsonPlaceHolderApi.class)
                .addCourse(course)
                .enqueue(new Callback<ResponseModel>() {
                    @Override
                    public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                        dialog.dismiss();
                        ResponseModel responseModel = response.body();
                        if (responseModel.getState().equals("SUCCESS")) {
                            Toast.makeText(AdminAddCourseActivity.this, "Add course success!!!", Toast.LENGTH_LONG).show();
                            startActivity(new Intent(AdminAddCourseActivity.this, AdminActivity.class));
                        } else {
                            Toast.makeText(AdminAddCourseActivity.this, "Add course failed!!!", Toast.LENGTH_LONG).show();
                        }

                    }

                    @Override
                    public void onFailure(Call<ResponseModel> call, Throwable t) {
                        dialog.dismiss();
                        Log.e("FAILURE: ", t.getMessage());
                    }
                });
    }
    @OnClick(R.id.btnCancel)
    public void back(View view) {
        onBackPressed();
    }
}
