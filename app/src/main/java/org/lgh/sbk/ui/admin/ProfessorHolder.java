package org.lgh.sbk.ui.admin;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import org.lgh.sbk.R;

public class ProfessorHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    ImageView imageView;
    TextView professorName, professorDescription;
    private ItemClickListener itemClickListener;
    CardView container;

    public ProfessorHolder(@NonNull View itemView) {
        super(itemView);
        container = itemView.findViewById(R.id.item_professor_relative);
        this.imageView = itemView.findViewById(R.id.imageTv);
        this.professorName = itemView.findViewById(R.id.professorName);
        this.professorDescription = itemView.findViewById(R.id.professorDescription);
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        this.itemClickListener.onItemClickListener(v, getLayoutPosition());
    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }
}
