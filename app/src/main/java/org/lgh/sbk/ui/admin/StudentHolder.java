package org.lgh.sbk.ui.admin;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import org.lgh.sbk.R;

public class StudentHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    ImageView imageView;
    TextView studentName, description;
    private ItemClickListener itemClickListener;
    CardView container;

    public StudentHolder(@NonNull View itemView) {
        super(itemView);
        container = itemView.findViewById(R.id.item_student_relative);
        this.imageView = itemView.findViewById(R.id.imageTv);
        this.studentName = itemView.findViewById(R.id.studentName);
        this.description = itemView.findViewById(R.id.description);
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        this.itemClickListener.onItemClickListener(v, getLayoutPosition());
    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }
}
