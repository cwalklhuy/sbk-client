package org.lgh.sbk.ui.admin;

import android.view.View;

public interface ItemClickListener {
    void onItemClickListener(View view, int position);
}
