package org.lgh.sbk.ui.admin;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.SearchView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.lgh.sbk.R;
import org.lgh.sbk.entity.Student;

import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AdminShowAllStudentActivity extends AppCompatActivity {

    @BindView(R.id.recycleViewStudent)
    RecyclerView recyclerView;
    StudentAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.admin_show_student_activity);
        ButterKnife.bind(this);

        recyclerView = findViewById(R.id.recycleViewStudent);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new StudentAdapter(this, getMyList());
        recyclerView.setAdapter(adapter);
    }

    @OnClick(R.id.btnCreateNewStudent)
    public void createStudent(View view) {
        Intent intent = new Intent(this, AdminAddStudentActivity.class);
        startActivity(intent);
    }

    private List<Student> getMyList() {
        List<Student> models = Arrays.asList(
                new Student(1, "Peter Johan", "Software Engineer", "14 November, 1998", "0123456789", "SE63125", true),
                new Student(2, "Peter Johan", "Software Engineer", "14 November, 1998", "0123456789", "SE63125", false),
                new Student(3, "Peter Johan", "Software Engineer", "14 November, 1998", "0123456789", "SE63125", true),
                new Student(4, "Peter Johan", "Software Engineer", "14 November, 1998", "0123456789", "SE63125", true),
                new Student(5, "Peter Johan", "Software Engineer", "14 November, 1998", "0123456789", "SE63125", true),
                new Student(6, "Peter Johan", "Software Engineer", "14 November, 1998", "0123456789", "SE63125", false),
                new Student(7, "Peter Johan", "Software Engineer", "14 November, 1998", "0123456789", "SE63125", false),
                new Student(8, "Peter Johan", "Software Engineer", "14 November, 1998", "0123456789", "SE63125", true),
                new Student(9, "Peter Johan", "Software Engineer", "14 November, 1998", "0123456789", "SE63125", false),
                new Student(10, "Peter Johan", "Software Engineer", "14 November, 1998", "0123456789", "SE63125", true),
                new Student(11, "Peter Johan", "Software Engineer", "14 November, 1998", "0123456789", "SE63125", false),
                new Student(12, "Peter Johan", "Software Engineer", "14 November, 1998", "0123456789", "SE63125", true),
                new Student(13, "Peter Johan", "Software Engineer", "14 November, 1998", "0123456789", "SE63125", false),
                new Student(14, "Peter Johan", "Software Engineer", "14 November, 1998", "0123456789", "SE63125", true),
                new Student(15, "Peter Johan", "Software Engineer", "14 November, 1998", "0123456789", "SE63125", false),
                new Student(15, "Peter Johan", "Software Engineer", "14 November, 1998", "0123456789", "SE63125", true),
                new Student(15, "Peter Johan", "Software Engineer", "14 November, 1998", "0123456789", "SE63125", true),
                new Student(15, "Peter Johan", "Software Engineer", "14 November, 1998", "0123456789", "SE63125", false),
                new Student(15, "Peter Johan", "Software Engineer", "14 November, 1998", "0123456789", "SE63125", true),
                new Student(15, "Peter Johan", "Software Engineer", "14 November, 1998", "0123456789", "SE63125", false),
                new Student(15, "Peter Johan", "Software Engineer", "14 November, 1998", "0123456789", "SE63125", true)
        );

        return models;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_search, menu);

        MenuItem item = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) item.getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                adapter.getFilter().filter(query);

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.getFilter().filter(newText);

                return false;
            }
        });

        return true;
    }
}
