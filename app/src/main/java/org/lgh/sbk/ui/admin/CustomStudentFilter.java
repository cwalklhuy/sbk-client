package org.lgh.sbk.ui.admin;

import android.widget.Filter;

import org.lgh.sbk.entity.Student;

import java.util.List;
import java.util.stream.Collectors;

public class CustomStudentFilter extends Filter {
    List<Student> filterList;
    StudentAdapter adapter;

    public CustomStudentFilter(List<Student> filterList, StudentAdapter adapter) {
        this.filterList = filterList;
        this.adapter = adapter;
    }

    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults results = new FilterResults();
        results.count = filterList.size();
        results.values = filterList;

        if (constraint != null && constraint.length() > 0) {
            List<Student> filterModels = filterList.stream()
                    .filter(model -> model.getFullname().toUpperCase().contains(constraint.toString().toUpperCase()))
                    .collect(Collectors.toList());
            results.count = filterModels.size();
            results.values = filterModels;
        }

        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {
        adapter.models = (List<Student>) results.values;
        adapter.notifyDataSetChanged();
    }
}
