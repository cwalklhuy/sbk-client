package org.lgh.sbk.ui;

import android.animation.ArgbEvaluator;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import org.lgh.sbk.R;
import org.lgh.sbk.ui.admin.AdminTaskAdapter;
import org.lgh.sbk.ui.admin.ItemTaskModel;

import java.util.ArrayList;
import java.util.List;

public class AdminActivity extends AppCompatActivity {
    ViewPager viewPager;
    AdminTaskAdapter adminTaskAdapter;
    List<ItemTaskModel> itemTaskModels;
    Integer[] colors = null;
    ArgbEvaluator argbEvaluator = new ArgbEvaluator();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);

        itemTaskModels = new ArrayList<>();
        itemTaskModels.add(new ItemTaskModel(R.drawable.calendar, "Calendar Management", ""));
        itemTaskModels.add(new ItemTaskModel(R.drawable.student, "Student Management", ""));
        itemTaskModels.add(new ItemTaskModel(R.drawable.course, "Section Management", ""));
        itemTaskModels.add(new ItemTaskModel(R.drawable.professor, "Professor Management", ""));
        itemTaskModels.add(new ItemTaskModel(R.drawable.course, "Course Management", ""));
        itemTaskModels.add(new ItemTaskModel(R.drawable.logout, "Logout", ""));

        adminTaskAdapter = new AdminTaskAdapter(itemTaskModels, this);

        viewPager = findViewById(R.id.viewPager);
        viewPager.setAdapter(adminTaskAdapter);
        viewPager.setPadding(130, 0, 130, 0);
    }

}
