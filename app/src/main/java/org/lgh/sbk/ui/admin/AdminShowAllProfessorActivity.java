package org.lgh.sbk.ui.admin;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.SearchView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.lgh.sbk.R;
import org.lgh.sbk.entity.Professor;
import org.lgh.sbk.entity.Student;

import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AdminShowAllProfessorActivity extends AppCompatActivity {

    @BindView(R.id.recycleViewProfessor)
    RecyclerView recyclerView;
    ProfessorAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.admin_show_professor_activity);
        ButterKnife.bind(this);

        recyclerView = findViewById(R.id.recycleViewProfessor);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new ProfessorAdapter(this, getMyList());
        recyclerView.setAdapter(adapter);
    }

    @OnClick(R.id.btnCreateNewStudent)
    public void createStudent(View view) {
        Intent intent = new Intent(this, AdminAddStudentActivity.class);
        startActivity(intent);
    }

    private List<Professor> getMyList() {
        List<Professor> models = Arrays.asList(
               new Professor(1, "Jea Trivedi", "JaeT", "Software Engineering", true),
               new Professor(1, "Jea Trivedi", "JaeT", "Software Engineering", true),
               new Professor(1, "Jea Trivedi", "JaeT", "Software Engineering", true),
               new Professor(1, "Jea Trivedi", "JaeT", "Software Engineering", true),
               new Professor(1, "Jea Trivedi", "JaeT", "Software Engineering", true),
               new Professor(1, "Jea Trivedi", "JaeT", "Software Engineering", true),
               new Professor(1, "Jea Trivedi", "JaeT", "Software Engineering", true),
               new Professor(1, "Jea Trivedi", "JaeT", "Software Engineering", true),
               new Professor(1, "Jea Trivedi", "JaeT", "Software Engineering", true),
               new Professor(1, "Jea Trivedi", "JaeT", "Software Engineering", true),
               new Professor(1, "Jea Trivedi", "JaeT", "Software Engineering", true),
               new Professor(1, "Jea Trivedi", "JaeT", "Software Engineering", true),
               new Professor(1, "Jea Trivedi", "JaeT", "Software Engineering", true),
               new Professor(1, "Jea Trivedi", "JaeT", "Software Engineering", true),
               new Professor(1, "Jea Trivedi", "JaeT", "Software Engineering", true),
               new Professor(1, "Jea Trivedi", "JaeT", "Software Engineering", true),
               new Professor(1, "Jea Trivedi", "JaeT", "Software Engineering", true)
        );

        return models;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_search, menu);

        MenuItem item = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) item.getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                adapter.getFilter().filter(query);

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.getFilter().filter(newText);

                return false;
            }
        });

        return true;
    }
}
