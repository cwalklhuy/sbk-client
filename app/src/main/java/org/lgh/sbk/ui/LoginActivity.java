package org.lgh.sbk.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import org.lgh.sbk.R;
import org.lgh.sbk.api.JsonPlaceHolderApi;
import org.lgh.sbk.api.common.ResponseModel;
import org.lgh.sbk.api.common.RetrofitClientInstance;
import org.lgh.sbk.entity.Student;
import org.lgh.sbk.entity.json.GetIAccountDetailsDeserializer;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    @BindView(R.id.username) EditText username;
    @BindView(R.id.password) EditText password;



    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.tvLogin)
    public void loginClick(View view) {

        ProgressDialog dialog = ProgressDialog.show(this, "Process...", "Please wait...", true);
        dialog.setMax(100);
        dialog.show();


        RetrofitClientInstance
                .getRetrofitInstance(ResponseModel.class, new GetIAccountDetailsDeserializer())
                .create(JsonPlaceHolderApi.class).getStudent(username.getText().toString(), password.getText().toString())
                .enqueue(new Callback<ResponseModel>() {
                    @Override
                    public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                        dialog.dismiss();
                        if (!response.isSuccessful()) {
                            Log.e("FAIL", "Unsuccessfully: " + response.code());
                        }

                        ResponseModel res = response.body();

                        if (res.getState().equals("SUCCESS")) {
                            Intent intent = new Intent(LoginActivity.this, AdminActivity.class);
                            if (res.getMsgId().equals("ROLE_USER")) {
                                intent = new Intent(LoginActivity.this, UserActivity.class);
                                intent.putExtra("studentLogin", (Student) res.getData());
                            }
                            startActivity(intent);
                        } else {
                            //ERROR_LOGIN
                            Toast.makeText(getBaseContext(), "Username or password incorrect", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseModel> call, Throwable t) {
                        dialog.dismiss();
                        Toast.makeText(getBaseContext(), t.getMessage(), Toast.LENGTH_LONG).show();
                        Log.e("FAIL", "onFailure: " + t.getMessage());
                    }
                });
    }
}

