package org.lgh.sbk.ui.admin;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.Filter;
import android.widget.Filterable;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.lgh.sbk.R;
import org.lgh.sbk.entity.Student;

import java.util.List;

public class StudentAdapter extends RecyclerView.Adapter<StudentHolder> implements Filterable {

    private Context context;
    List<Student> models, filterList;
    private CustomStudentFilter filter;

    public StudentAdapter(Context context, List<Student> models) {
        this.context = context;
        this.models = models;
        this.filterList = models;
    }

    @NonNull
    @Override
    public StudentHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.student_row, null);

        return new StudentHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final StudentHolder holder, int position) {
        Student student = models.get(position);
        String des = String.format("Student No. : %s\nMajor: %s", student.getStudentNumber(), student.getMajor());

        holder.studentName.setText(student.getFullname());
        holder.description.setText(des);
        holder.imageView.setImageResource(student.isGender() ? R.drawable.man : R.drawable.woman);
        holder.imageView.setAnimation(AnimationUtils.loadAnimation(context, R.anim.fade_transition_animation));
        holder.container.setAnimation(AnimationUtils.loadAnimation(context, R.anim.fade_scale_animation));

        holder.setItemClickListener((view, position1) -> {

        });
    }

    @Override
    public int getItemCount() {
        return models.size();
    }

    @Override
    public Filter getFilter() {
        if (filter == null)
            filter = new CustomStudentFilter(filterList, this);

        return filter;
    }
}
