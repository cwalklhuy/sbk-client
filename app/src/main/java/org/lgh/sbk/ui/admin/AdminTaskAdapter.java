package org.lgh.sbk.ui.admin;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import org.lgh.sbk.R;

import java.util.List;

public class AdminTaskAdapter extends PagerAdapter {

    private List<ItemTaskModel> itemTaskModels;
    private LayoutInflater layoutInflater;
    private Context context;

    public AdminTaskAdapter(List<ItemTaskModel> itemTaskModels, Context context) {
        this.itemTaskModels = itemTaskModels;
        this.context = context;
    }

    @Override
    public int getCount() {
        return itemTaskModels.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view.equals(object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, final int position) {
        layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.admin_task_item, container, false);

        ImageView imageView;
        TextView title, description;

        imageView = view.findViewById(R.id.image);
        title = view.findViewById(R.id.title);
        description = view.findViewById(R.id.description);

        imageView.setImageResource(itemTaskModels.get(position).getImage());
        title.setText(itemTaskModels.get(position).getTitle());
        title.setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/Poppins-Regular.ttf"));
        description.setText(itemTaskModels.get(position).getDescription());
        description.setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/Poppins-Regular.ttf"));

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = null;
                String activity = itemTaskModels.get(position).getTitle();

                switch (activity) {
                    case "Calendar Management": {
                        intent = new Intent(context, AdminAddScheduleActivity.class);
                        break;
                    }

                    case "Student Management": {
                        intent = new Intent(context, AdminShowAllStudentActivity.class);
                        break;
                    }

                    case "Section Management": {
                        intent = new Intent(context, AdminAddSectionActivity.class);
                        break;
                    }

                    case "Professor Management": {
                        intent = new Intent(context, AdminShowAllProfessorActivity.class);
                        break;
                    }

                    case "Course Management": {
                        intent = new Intent(context, AdminAddCourseActivity.class);
                        break;
                    }
                    default: intent = new Intent(context, null);
                }

                context.startActivity(intent);
            }
        });

        container.addView(view, 0);
        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View)object);
    }
}
