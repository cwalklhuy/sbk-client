package org.lgh.sbk.ui.admin;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import org.lgh.sbk.R;
import org.lgh.sbk.api.JsonPlaceHolderApi;
import org.lgh.sbk.api.common.ResponseModel;
import org.lgh.sbk.api.common.RetrofitClientInstance;
import org.lgh.sbk.entity.Student;
import org.lgh.sbk.entity.json.GetIAccountDetailsDeserializer;
import org.lgh.sbk.ui.AdminActivity;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AdminAddStudentActivity extends AppCompatActivity {

    @BindView(R.id.fullName)
    EditText fullName;
    @BindView(R.id.dob)
    EditText edDob;
    @BindView(R.id.phone)
    EditText phone;
    @BindView(R.id.spinnerMajor)
    Spinner spinnerMajor;

    String[] majors = {
            "SE", "SB"
    };
    String major = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_add_student);
        ButterKnife.bind(this);
        ArrayAdapter<String> adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, majors);

        spinnerMajor.setAdapter(adapter);
        spinnerMajor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                major = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    @OnClick(R.id.btnCreateStudent)
    public void createStudent(View view) {

        Student student = Student.builder()
                .fullname(fullName.getText().toString())
                .major(major)
                .dob(edDob.getText().toString())
                .phoneNumber(phone.getText().toString())
                .build();



        // Set up progress before call
        final ProgressDialog dialog = ProgressDialog.show(this, "Process..", "Please wait...", true);
        dialog.setMax(100);
        // show it
        dialog.show();
        RetrofitClientInstance
                .getRetrofitInstance(ResponseModel.class, new GetIAccountDetailsDeserializer())
                .create(JsonPlaceHolderApi.class)
                .addStudent(student)
                .enqueue(new Callback<ResponseModel>() {
                    @Override
                    public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                        dialog.dismiss();
                        if (!response.isSuccessful()) {
                            Log.e("FAIL", "Unsuccessfull: " + response.code());
                        }
                        ResponseModel responseModel = response.body();
                        String state = responseModel.getState();
                        Student student1 = (Student) responseModel.getData();
                        if (state.equals("SUCCESS")) {
                           Toast.makeText(getBaseContext(), "Add success: " + student1.getId(), Toast.LENGTH_LONG).show();
                           Intent intent = new Intent(AdminAddStudentActivity.this, AdminActivity.class);
                           startActivity(intent);
                        } else {
                            Toast.makeText(getBaseContext(), "Add fail", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseModel> call, Throwable t) {
                        dialog.dismiss();
                        Log.e("FAIL", "Failure: " + t.getMessage());
                    }
                });
    }

    @OnClick(R.id.btnCancel)
    public void back(View view) {
        onBackPressed();
    }
}
