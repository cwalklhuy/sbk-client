package org.lgh.sbk.ui.admin;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import org.lgh.sbk.R;
import org.lgh.sbk.api.JsonPlaceHolderApi;
import org.lgh.sbk.api.common.ResponseModel;
import org.lgh.sbk.api.common.RetrofitClientInstance;
import org.lgh.sbk.entity.Professor;
import org.lgh.sbk.ui.AdminActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AdminAddProfessorActivity extends AppCompatActivity {

    @BindView(R.id.nameTeacher)
    EditText nameTeacher;
    @BindView(R.id.spinnerDepartment)
    Spinner spinnerDepartment;
    @BindView(R.id.titleTeacher)
    EditText titleTeacher;

    String[] departments = {
            "Software Engineer",
            "Information Assurance"
    };
    String departmentChecked = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_add_professor);
        ButterKnife.bind(this);
        ArrayAdapter<String> adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, departments);
        spinnerDepartment.setAdapter(adapter);
        spinnerDepartment.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                departmentChecked = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @OnClick(R.id.btnCreateProfessor)
    public void createProfessor(View view) {
        Professor professor = Professor.builder()
                .department(departmentChecked)
                .name(nameTeacher.getText().toString())
                .title(titleTeacher.getText().toString())
                .build();
        final ProgressDialog dialog = ProgressDialog.show(this, "Process...", "Please wait...", true);
        dialog.setMax(100);
        dialog.show();

        RetrofitClientInstance
                .getRetrofitInstance()
                .create(JsonPlaceHolderApi.class)
                .addProfessor(professor)
                .enqueue(new Callback<ResponseModel>() {
                    @Override
                    public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                        dialog.dismiss();
                        Toast.makeText(getBaseContext(), "Add professor success", Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(AdminAddProfessorActivity.this, AdminActivity.class);
                        startActivity(intent);
                    }

                    @Override
                    public void onFailure(Call<ResponseModel> call, Throwable t) {
                        dialog.dismiss();
                        Log.e("FAILURE", "On Failure: " + t.getMessage());
                    }
                });
    }

    @OnClick(R.id.btnCancel)
    public void back(View view) {
        onBackPressed();
    }
}
