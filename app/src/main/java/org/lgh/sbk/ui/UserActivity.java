package org.lgh.sbk.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.google.android.material.navigation.NavigationView;

import org.lgh.sbk.R;
import org.lgh.sbk.entity.Student;
import org.lgh.sbk.fragments.AccountDetailFragment;
import org.lgh.sbk.fragments.ChooseWeekFragment;
import org.lgh.sbk.fragments.ScheduleTodayFragment;
import org.lgh.sbk.fragments.ScheduleWeeklyFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class UserActivity extends AppCompatActivity {


    @BindView(R.id.drawer) DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mToggle;
    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.nav_view) NavigationView navigationView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);
        ButterKnife.bind(this);
        Intent intent = getIntent();
        Student studentLogin = (Student) intent.getSerializableExtra("studentLogin");
        View hView = navigationView.getHeaderView(0);
        TextView studentName = hView.findViewById(R.id.studentFullName);
        TextView studentCode = hView.findViewById(R.id.studentCode);
        studentName.setText(studentLogin.getFullname());
        studentCode.setText(studentLogin.getStudentNumber());
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.nav_week:
                        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                                new ChooseWeekFragment()).commit();
                        break;
                    case R.id.nav_today:
                        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                                new ScheduleTodayFragment()).commit();
                        break;
                        case R.id.nav_info_student:
                        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                                new AccountDetailFragment()).commit();
                        break;
                    case R.id.nav_logout:
                        Intent intent = new Intent(UserActivity.this, MainActivity.class);
                        startActivity(intent);
                }
                mDrawerLayout.closeDrawer(GravityCompat.START);
                return true;
            }
        });
        toolbar.setTitle("Student's Home");
        mToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.open, R.string.close);
        mDrawerLayout.addDrawerListener(mToggle);
        mToggle.syncState();
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                    new ScheduleTodayFragment()).commit();
            navigationView.setCheckedItem(R.id.nav_today);
        }
    }


    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }

    }

    public void clickToViewWeek(View view) {
        ScheduleWeeklyFragment fragment = new ScheduleWeeklyFragment(view.getTag().toString());
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                fragment).commit();
    }
}
