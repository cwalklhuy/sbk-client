package org.lgh.sbk.ui.admin;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.reflect.TypeToken;

import org.lgh.sbk.R;
import org.lgh.sbk.api.JsonPlaceHolderApi;
import org.lgh.sbk.api.common.ResponseModel;
import org.lgh.sbk.api.common.RetrofitClientInstance;
import org.lgh.sbk.entity.Course;
import org.lgh.sbk.entity.Professor;
import org.lgh.sbk.entity.json.GetISectionListDeserializer;
import org.lgh.sbk.entity.json.SectionCreate;
import org.lgh.sbk.ui.AdminActivity;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AdminAddSectionActivity extends AppCompatActivity {
    @BindView(R.id.course_spinner)
    Spinner courseSpinner;
    @BindView(R.id.professor_spinner)
    Spinner professorSpinner;
    @BindView(R.id.sectionNo)
    TextView sectionNo;
    @BindView(R.id.capacity)
    TextView capacity;
    String course = "";
    String professor = "";
    List<String> listData = new ArrayList<>();
    ArrayAdapter<String> arrayAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_add_section);

        ButterKnife.bind(this);
        initCourse();
        initProfessor();

        courseSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                course = (String) parent.getItemAtPosition(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        professorSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                professor = (String) parent.getItemAtPosition(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }


    @OnClick(R.id.btnCreateSection)
    public void createSection(View view) {
        SectionCreate section = SectionCreate.builder()
                .course(course)
                .professor(professor)
                .sectionNo(sectionNo.getText().toString())
                .capacity(Integer.parseInt(capacity.getText().toString()))
                .build();
        RetrofitClientInstance
                .getRetrofitInstance()
                .create(JsonPlaceHolderApi.class)
                .makeSection(section)
                .enqueue(new Callback<ResponseModel>() {
                    @Override
                    public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                        if (!response.isSuccessful()) {
                            Log.e("FAIL", "Unsuccessfully: " + response.code());
                        }
                        ResponseModel responseModel = response.body();
                        if (responseModel.getState().equals("SUCCESS")) {
                            Toast.makeText(AdminAddSectionActivity.this, responseModel.getMsgId(), Toast.LENGTH_LONG).show();
                            Intent intent = new Intent(AdminAddSectionActivity.this, AdminActivity.class);
                        } else {
                            Toast.makeText(AdminAddSectionActivity.this, responseModel.getMsgId(), Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseModel> call, Throwable t) {

                    }
                });
    }


    private void initCourse() {
        RetrofitClientInstance
                .getRetrofitInstance(new TypeToken<List<Course>>(){}.getType(), new GetISectionListDeserializer())
                .create(JsonPlaceHolderApi.class)
                .getCourse()
                .enqueue(new Callback<List<Course>>() {
                    @Override
                    public void onResponse(Call<List<Course>> call, Response<List<Course>> response) {
                        if (!response.isSuccessful()) {
                            Log.e("FAIL", "Unsuccessfully: " + response.code());
                        }
                        List<Course> courses = response.body();
                        listData.clear();
                        listData.addAll(courses.stream()
                                .map(Course::getCourseCode)
                                .collect(Collectors.toList()));
                        arrayAdapter = new ArrayAdapter<>(AdminAddSectionActivity.this, android.R.layout.simple_spinner_item, listData);
                        courseSpinner.setAdapter(arrayAdapter);
                    }

                    @Override
                    public void onFailure(Call<List<Course>> call, Throwable t) {
                        Log.e("onFailure", t.getMessage());
                    }
                });
    }

    private void initProfessor() {
        RetrofitClientInstance
                .getRetrofitInstance(new TypeToken<List<Professor>>(){}.getType(), new GetISectionListDeserializer())
                .create(JsonPlaceHolderApi.class)
                .getProfessors()
                .enqueue(new Callback<List<Professor>>() {
                    @Override
                    public void onResponse(Call<List<Professor>> call, Response<List<Professor>> response) {
                        if (!response.isSuccessful()) {
                            Log.e("FAIL", "Unsuccessfully: " + response.code());
                        }
                        List<Professor> professors = response.body();
                        listData.clear();
                        listData.addAll(professors.stream()
                                .map(Professor::getTitle)
                                .collect(Collectors.toList()));
                        arrayAdapter = new ArrayAdapter<>(AdminAddSectionActivity.this, android.R.layout.simple_spinner_item, listData);
                        professorSpinner.setAdapter(arrayAdapter);
                    }

                    @Override
                    public void onFailure(Call<List<Professor>> call, Throwable t) {
                        Log.e("onFailure", t.getMessage());
                    }
                });
    }
}
