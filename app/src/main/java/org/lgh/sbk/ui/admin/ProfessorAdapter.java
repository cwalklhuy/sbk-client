package org.lgh.sbk.ui.admin;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.Filter;
import android.widget.Filterable;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.lgh.sbk.R;
import org.lgh.sbk.entity.Professor;

import java.util.List;

public class ProfessorAdapter extends RecyclerView.Adapter<ProfessorHolder> implements Filterable {

    private Context context;
    List<Professor> models, filterList;
    private CustomProfessorFilter filter;

    public ProfessorAdapter(Context context, List<Professor> models) {
        this.context = context;
        this.models = models;
        this.filterList = models;
    }

    @NonNull
    @Override
    public ProfessorHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.professor_row, null);

        return new ProfessorHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ProfessorHolder holder, int position) {
        Professor professor = models.get(position);
        String des = String.format("Title: %s\nDepartment: %s", professor.getTitle(), professor.getDepartment());

        holder.professorName.setText(professor.getName());
        holder.professorDescription.setText(des);
        holder.imageView.setImageResource(professor.isGender() ? R.drawable.man : R.drawable.woman);
        holder.imageView.setAnimation(AnimationUtils.loadAnimation(context, R.anim.fade_transition_animation));
        holder.container.setAnimation(AnimationUtils.loadAnimation(context, R.anim.fade_scale_animation));

        holder.setItemClickListener((view, position1) -> {

        });
    }

    @Override
    public int getItemCount() {
        return models.size();
    }

    @Override
    public Filter getFilter() {
        if (filter == null)
            filter = new CustomProfessorFilter(filterList, this);

        return filter;
    }
}
