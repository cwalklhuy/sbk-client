package org.lgh.sbk.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.reflect.TypeToken;

import org.lgh.sbk.R;
import org.lgh.sbk.adapter.ScheduleWeeklyAdapter;
import org.lgh.sbk.api.JsonPlaceHolderApi;
import org.lgh.sbk.api.common.RetrofitClientInstance;
import org.lgh.sbk.entity.Schedule;
import org.lgh.sbk.entity.ScheduleCustom;
import org.lgh.sbk.entity.Student;
import org.lgh.sbk.entity.json.GetIScheduleListDetailsDeserializer;
import org.lgh.sbk.ui.WeeklyDetailsActivity;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

import butterknife.OnItemClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static java.util.Map.Entry.comparingByKey;
import static java.util.Map.Entry.comparingByValue;


/**
 * A simple {@link Fragment} subclass.
 */
public class ScheduleWeeklyFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener{

    TextView fromDate;
    TextView toDate;
    ProgressBar spinner;
    String dateInfo;
    int studentId = 0;
    ListView listView;
    SwipeRefreshLayout swipeRefreshLayout;
    ScheduleWeeklyAdapter adapter;
    TextView responseState;
    //Lịch weekly để hiển thị là đây nhé các fen.
    List<ScheduleCustom> scheduleCustoms = new ArrayList<>();

    public ScheduleWeeklyFragment() {
        // Required empty public constructor
    }

    public ScheduleWeeklyFragment(String dateInfo) {
        this.dateInfo = dateInfo;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_schedule_weekly, container, false);
        toDate = view.findViewById(R.id.toDayy);
        fromDate = view.findViewById(R.id.frDay);
        listView = view.findViewById(R.id.list_view_weekly);
        swipeRefreshLayout = view.findViewById(R.id.refreshWeekly);
        spinner = view.findViewById(R.id.progressBar2);
        responseState = view.findViewById(R.id.responseState);
        Intent intent = getActivity().getIntent();
        Student studentLogin = (Student) intent.getSerializableExtra("studentLogin");
        studentId = studentLogin.getId();
       String[] dateStr = dateInfo.split("-");
        toDate.setText(dateStr[1]);
        fromDate.setText(dateStr[0]);
        try {
            initScheduleRange(studentId);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ScheduleCustom scheduleCustom = (ScheduleCustom) parent.getItemAtPosition(position);
                Intent intent = new Intent(getContext(), WeeklyDetailsActivity.class);
                intent.putExtra("scheduleDetail", scheduleCustom);
                startActivity(intent);
            }
        });

        swipeRefreshLayout.setOnRefreshListener(this);

        return view;
    }

    private void initScheduleRange(int studentId) throws ParseException {
        String endTime = converFormatDate(toDate.getText().toString());
        String startTime = converFormatDate(fromDate.getText().toString());
        RetrofitClientInstance
                .getRetrofitInstance(new TypeToken<List<Schedule>>(){}.getType(), new GetIScheduleListDetailsDeserializer())
                .create(JsonPlaceHolderApi.class)
                .getAllScheduleRange(studentId, startTime, endTime)
                .enqueue(new Callback<List<Schedule>>() {
                    @Override
                    public void onResponse(Call<List<Schedule>> call, Response<List<Schedule>> response) {
                        spinner.setVisibility(View.GONE);
                        if (!response.isSuccessful()) {
                            Log.e("FAIL", "Unsuccessfull: " + response.code());
                        }
                        if (getContext() != null) {
                            List<Schedule> listValue = response.body();
                            if (listValue.isEmpty()) {
                                responseState.setText("Nothing to show!");
                            } else {
                                listValue.stream()
                                        .collect(Collectors.groupingBy(Schedule::getDate))
                                        .entrySet().stream()
                                        .sorted(comparingByKey())
                                        .forEach(e -> {
                                            scheduleCustoms.add(new ScheduleCustom(e.getKey(), e.getValue()));
                                        });
                                adapter = new ScheduleWeeklyAdapter(getContext(), scheduleCustoms);
                                listView.setAdapter(adapter);
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<List<Schedule>> call, Throwable t) {
                        spinner.setVisibility(View.GONE);
                        Log.e("onFailure", t.getMessage());
                    }
                });
    }

    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(true);
        scheduleCustoms.clear();
        spinner.setVisibility(View.VISIBLE);
        try {
            initScheduleRange(studentId);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        refreshList();
    }

    private void refreshList() {
        swipeRefreshLayout.setRefreshing(false);
    }

    // Chuyển đổi định dạng từ màn hình chọn lịch (chooseWeeklyFragment)
    private String converFormatDate(String dateInfo) throws ParseException {
        final DateFormat orginalFormat = new SimpleDateFormat("dd MMM, yyyy", Locale.ENGLISH);
        final DateFormat targetFormat = new SimpleDateFormat("yyyy-MM-dd");
        final Date date = orginalFormat.parse(dateInfo);
        final String formatedDate = targetFormat.format(date);
        return formatedDate;
    }
}