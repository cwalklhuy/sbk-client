package org.lgh.sbk.fragments;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.savvi.rangedatepicker.CalendarPickerView;

import org.lgh.sbk.R;
import org.lgh.sbk.ui.UserActivity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ChooseWeekFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ChooseWeekFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ChooseWeekFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

//    @BindView(R.id.calendar_view)
    CalendarPickerView calendar;
//    @BindView(R.id.fromDay)
    TextView fromDate;
//    @BindView(R.id.toDay)
    TextView toDate;
//    @BindView(R.id.btNext)
    Button btnNext;
    private ArrayList<Date> range = new ArrayList<>();

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    public ChooseWeekFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ChooseWeekFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ChooseWeekFragment newInstance(String param1, String param2) {
        ChooseWeekFragment fragment = new ChooseWeekFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_choose_week, container, false);
        ButterKnife.bind(this, view);

        btnNext = view.findViewById(R.id.btNext);
        toDate = view.findViewById(R.id.toDay);
        fromDate = view.findViewById(R.id.fromDay);
        calendar = view.findViewById(R.id.calendar_view);
        Calendar pastYear = Calendar.getInstance();
        pastYear.add(Calendar.YEAR, -1);
        Calendar nextYear = Calendar.getInstance();
        nextYear.add(Calendar.DATE, 300);
//        nextYear.add(Calendar.DATE, 1);
//        nextYear.add(Calendar.DATE,1);
        calendar.init(pastYear.getTime(), nextYear.getTime()) //
                .inMode(CalendarPickerView.SelectionMode.SINGLE)
                .withSelectedDate(new Date());
        calendar.setTypeface(Typeface.SANS_SERIF);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        calendar.setOnDateSelectedListener(new CalendarPickerView.OnDateSelectedListener() {
            @Override
            public void onDateSelected(Date date) {
                ArrayList<Date> range7days = new ArrayList<>();
                calendar.clearHighlightedDates();
                range = range7days;
                Date dt = calendar.getSelectedDate();
                range.add(calendar.getSelectedDate());
                for (int i = 0; i < 6; i++) {
                    Calendar selectedDate = toCalendar(dt);
                    selectedDate.add(Calendar.DAY_OF_YEAR, 1);
                    Date tomorrow = selectedDate.getTime();
                    range.add(tomorrow);
                    dt = tomorrow;
                }
                calendar.highlightDates(range);
                //format date
                String strFromDate = formatDate(range.get(0).getTime(), "dd MMM", true);
                String strToDate = formatDate(range.get(range.size() - 1).getTime(), "dd MMM", true);
                btnNext.setTag(strFromDate+"-"+strToDate);
                fromDate.setText(range.size() > 0 ? (strFromDate) : "- -");
                toDate.setText(range.size() > 0 ? (strToDate) : "- -");

            }

            @Override
            public void onDateUnselected(Date date) {
            }
        });

    }

    private Calendar toCalendar(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal;
    }

    private static String formatDate(long date_millis, String format, boolean canAddYear) {
        if (canAddYear) {
            format = format + ", yyyy ";
        }
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        return simpleDateFormat.format(date_millis);
    }
}