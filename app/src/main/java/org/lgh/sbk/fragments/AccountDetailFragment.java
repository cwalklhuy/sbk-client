package org.lgh.sbk.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.lgh.sbk.R;
import org.lgh.sbk.entity.Student;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link AccountDetailFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link AccountDetailFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AccountDetailFragment extends Fragment {

    @BindView(R.id.txtStuNumber)
    TextView txtStuNumber;
    @BindView(R.id.txtStuName)
    TextView txtStuName;
    @BindView(R.id.txtStuMajor)
    TextView txtStuMajor;
    @BindView(R.id.txtStuDob)
    TextView txtStuDob;
    @BindView(R.id.txtStuPhone)
    TextView txtStuPhone;

    private OnFragmentInteractionListener mListener;

    public AccountDetailFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_account_detail, container, false);
        ButterKnife.bind(this, view);
        Intent intent = getActivity().getIntent();
        Student studentLogin = (Student) intent.getSerializableExtra("studentLogin");
        txtStuMajor.setText(studentLogin.getMajor());
        txtStuNumber.setText(studentLogin.getStudentNumber());
        txtStuDob.setText(studentLogin.getDob());
        txtStuName.setText(studentLogin.getFullname());
        txtStuPhone.setText(studentLogin.getPhoneNumber());
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }



    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
