package org.lgh.sbk.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.reflect.TypeToken;

import org.lgh.sbk.R;
import org.lgh.sbk.adapter.ScheduleAdapter;
import org.lgh.sbk.api.JsonPlaceHolderApi;
import org.lgh.sbk.api.common.RetrofitClientInstance;
import org.lgh.sbk.entity.Schedule;
import org.lgh.sbk.entity.Student;
import org.lgh.sbk.entity.json.GetIScheduleListDetailsDeserializer;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class ScheduleTodayFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.list_view)
    ListView listView;

    List<Schedule> scheduleList = new ArrayList<>();

    @BindView(R.id.txtToday)
    TextView txtToday;
    @BindView(R.id.progressBar1)
    ProgressBar spinner;
    int studentId = 0;
    @BindView(R.id.refresh)
    SwipeRefreshLayout swipeRefreshLayout;
    ScheduleAdapter scheduleAdapter;

    public ScheduleTodayFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_schedule_today, container, false);
        ButterKnife.bind(this, view);
        Intent intent = getActivity().getIntent();
        Student studentLogin = (Student) intent.getSerializableExtra("studentLogin");
        studentId = studentLogin.getId();
        spinner.setVisibility(View.VISIBLE);
//        DateTimeFormatter dtf = DateTimeFormatter.ofPattern(" dd MMM, yyyy");
//        LocalDateTime now = LocalDateTime.now();
        String current = new SimpleDateFormat("dd MMM, yyyy").format(Calendar.getInstance().getTime());
//        txtToday.setText(dtf.format(now));
        txtToday.setText(current);
        initCurrentDate();

        swipeRefreshLayout.setOnRefreshListener(this);
        return view;
    }

    private void initCurrentDate() {
        //        Gọi api để lấy lịch học hôm nay
        RetrofitClientInstance
                .getRetrofitInstance(new TypeToken<List<Schedule>>(){}.getType(), new GetIScheduleListDetailsDeserializer())
                .create(JsonPlaceHolderApi.class)
                .getAllScheduleToday(studentId)
                .enqueue(new Callback<List<Schedule>>() {
                    @Override
                    public void onResponse(Call<List<Schedule>> call, Response<List<Schedule>> response) {
                        spinner.setVisibility(View.GONE);
                        if (!response.isSuccessful()) {
                            Log.e("FAIL", "Unsuccessfull: " + response.code());
                        }
                        List<Schedule> listValue = response.body();
                        scheduleList.addAll(listValue);
                        if (getContext() != null) {
                            scheduleAdapter = new ScheduleAdapter(getContext(), scheduleList);
                            listView.setAdapter(scheduleAdapter);
                        }
                    }

                    @Override
                    public void onFailure(Call<List<Schedule>> call, Throwable t) {
                        spinner.setVisibility(View.GONE);
                        Log.e("onFailure", t.getMessage());
                    }
                });
    }

    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(true);
        scheduleList.clear();
        spinner.setVisibility(View.VISIBLE);
        initCurrentDate();
        refreshList();
    }

    private void refreshList() {
        swipeRefreshLayout.setRefreshing(false);
    }
}
