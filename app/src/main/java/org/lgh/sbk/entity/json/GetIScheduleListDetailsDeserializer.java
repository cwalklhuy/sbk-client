package org.lgh.sbk.entity.json;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import org.lgh.sbk.api.common.ResponseModel;
import org.lgh.sbk.entity.Schedule;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class GetIScheduleListDetailsDeserializer implements JsonDeserializer<List<Schedule>> {

    @Override
    public List deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        List scheduleList = new ArrayList<>();
        JsonObject jsonObject = json.getAsJsonObject();
        final String msgId = jsonObject.get("msgId").getAsString();
        final String state = jsonObject.get("state").getAsString();
        if (state.equals("SUCCESS")) {
            JsonArray jsonArray = jsonObject.get("data").getAsJsonArray();
            for (JsonElement jsonElement : jsonArray) {
                JsonObject itemJsonObject = jsonElement.getAsJsonObject();
                int attendanceId = itemJsonObject.get("attendanceId").getAsInt();
                String room = itemJsonObject.get("room").getAsString();
                String status = itemJsonObject.get("status").getAsString();
                String groupStudent = itemJsonObject.get("groupStudent").getAsString();
                String date = itemJsonObject.get("date").getAsString();
                String slotName = itemJsonObject.get("slotName").getAsString();
                String startTime = itemJsonObject.get("startTime").getAsString();
                String endTime = itemJsonObject.get("endTime").getAsString();
                int instructorId = itemJsonObject.get("instructorId").getAsInt();
                String instructorTitle = itemJsonObject.get("instructorTitle").getAsString();
                String instructorName = itemJsonObject.get("instructorName").getAsString();
                String instructorDepartment = itemJsonObject.get("instructorDepartment").getAsString();
                String courseCode = itemJsonObject.get("courseCode").getAsString();
                String courseName = itemJsonObject.get("courseName").getAsString();

                Schedule schedule = Schedule.builder()
                        .attendanceId(attendanceId)
                        .room(room)
                        .status(status)
                        .groupStudent(groupStudent)
                        .date(date)
                        .slotName(slotName)
                        .startTime(startTime)
                        .endTime(endTime)
                        .instructorId(instructorId)
                        .instructorTitle(instructorTitle)
                        .instructorName(instructorName)
                        .instructorDepartment(instructorDepartment)
                        .courseCode(courseCode)
                        .courseName(courseName)
                        .build();
                scheduleList.add(schedule);
            }
        }
        return scheduleList;
    }
}
