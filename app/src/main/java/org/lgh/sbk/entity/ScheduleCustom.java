package org.lgh.sbk.entity;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ScheduleCustom implements Serializable {
    private String date;
    private List<Schedule> scheduleList;

    public String converDayOfWeek() throws ParseException {
        SimpleDateFormat simpleDateformat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = simpleDateformat.parse(this.date);
        simpleDateformat = new SimpleDateFormat("E");
        String dateOfWeekFormat = simpleDateformat.format(date);
        return dateOfWeekFormat;
    }

    public String converDayOfMonth() throws ParseException {
        SimpleDateFormat simpleDateformat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = simpleDateformat.parse(this.date);
        simpleDateformat = new SimpleDateFormat("dd");
        String dayOfMonthFormat = simpleDateformat.format(date);
        return dayOfMonthFormat;
    }
}
