package org.lgh.sbk.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Schedule implements Serializable {

    @SerializedName("attendanceId")
    @Expose
    private int attendanceId;
    @SerializedName("room")
    @Expose
    private String room;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("groupStudent")
    @Expose
    private String groupStudent;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("slotName")
    @Expose
    private String slotName;
    @SerializedName("startTime")
    @Expose
    private String startTime;
    @SerializedName("endTime")
    @Expose
    private String endTime;
    @SerializedName("instructorId")
    @Expose
    private int instructorId;
    @SerializedName("instructorTitle")
    @Expose
    private String instructorTitle;
    @SerializedName("instructorName")
    @Expose
    private String instructorName;
    @SerializedName("instructorDepartment")
    @Expose
    private String instructorDepartment;
    @SerializedName("courseCode")
    @Expose
    private String courseCode;
    @SerializedName("courseName")
    @Expose
    private String courseName;
}
