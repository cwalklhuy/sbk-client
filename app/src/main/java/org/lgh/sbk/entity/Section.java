package org.lgh.sbk.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Section implements Serializable {


    @SerializedName("sectionId")
    @Expose
    private int sectionId;
    @SerializedName("sectionNo")
    @Expose
    private String sectionNo;

}
