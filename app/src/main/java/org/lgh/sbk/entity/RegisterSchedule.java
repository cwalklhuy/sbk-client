package org.lgh.sbk.entity;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class RegisterSchedule implements Serializable {
    @SerializedName("sectionNo")
    private String sectionNo;
    @SerializedName("room")
    private String room;
    @SerializedName("dates")
    private List<String> dates;
    @SerializedName("slotName")
    private String slotName;
    @SerializedName("studentNumbers")
    private List<String> studentNumbers;
}
