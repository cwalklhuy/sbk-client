package org.lgh.sbk.entity.json;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import org.lgh.sbk.entity.Section;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class GetISectionListDeserializer implements JsonDeserializer<List<Section>> {


    @Override
    public List deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        List sectionList = new ArrayList<>();
        JsonObject jsonObject = json.getAsJsonObject();
        final String msgId = jsonObject.get("msgId").getAsString();
        final String state = jsonObject.get("state").getAsString();
        if (state.equals("SUCCESS")) {
            JsonArray jsonArray = jsonObject.get("data").getAsJsonArray();
            for (JsonElement jsonElement : jsonArray) {
                JsonObject itemJsonObject = jsonElement.getAsJsonObject();
                int sectionId = itemJsonObject.get("sectionId").getAsInt();
                String sectionNo = itemJsonObject.get("sectionNo").getAsString();
                Section section = Section.builder()
                        .sectionId(sectionId)
                        .sectionNo(sectionNo)
                        .build();
                sectionList.add(section);
            }
        }
        return sectionList;
    }
}
