package org.lgh.sbk.entity.json;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SectionCreate {

    @SerializedName("course")
    @Expose
    private String course;

    @SerializedName("professor")
    @Expose
    private String professor;

    @SerializedName("sectionNo")
    @Expose
    private String sectionNo;

    @SerializedName("capacity")
    @Expose
    private int capacity;
}
