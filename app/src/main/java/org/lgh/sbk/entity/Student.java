package org.lgh.sbk.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Student implements Serializable {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("fullname")
    @Expose
    private String fullname;
    @SerializedName("major")
    @Expose
    private String major;
    @SerializedName("dob")
    @Expose
    private String dob;
    @SerializedName("phoneNumber")
    @Expose
    private String phoneNumber;
    @SerializedName("studentNumber")
    @Expose
    private String studentNumber;
    @SerializedName("gender")
    @Expose
    private boolean gender;
}
