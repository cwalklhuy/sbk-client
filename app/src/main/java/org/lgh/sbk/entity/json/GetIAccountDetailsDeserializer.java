package org.lgh.sbk.entity.json;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import org.lgh.sbk.api.common.ResponseModel;
import org.lgh.sbk.entity.Account;
import org.lgh.sbk.entity.Student;

import java.lang.reflect.Type;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class GetIAccountDetailsDeserializer implements JsonDeserializer<ResponseModel> {
    @Override
    public ResponseModel deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        final JsonObject jsonObject = json.getAsJsonObject();
        final String msgId = jsonObject.get("msgId").getAsString();
        final String state = jsonObject.get("state").getAsString();
        ResponseModel res = new ResponseModel();
        res.setMsgId(msgId);
        res.setState(state);
        if (state.equals("SUCCESS")) {
            JsonObject accountJsonObject = jsonObject.get("data").getAsJsonObject();
            //Set Account
            final int id = accountJsonObject.get("studentId").getAsInt();
            final String fullName = accountJsonObject.get("fullname").getAsString();
            final String major = accountJsonObject.get("major").getAsString();
            final String phoneNumber = accountJsonObject.get("phoneNumber").getAsString();
            final String studentNumber = accountJsonObject.get("studentNumber").getAsString();
            final String dobJs = accountJsonObject.get("dob").getAsString();
            Student student = Student.builder()
                    .id(id)
                    .fullname(fullName)
                    .phoneNumber(phoneNumber)
                    .dob(dobJs)
                    .major(major)
                    .studentNumber(studentNumber)
                    .build();
            res.setData(student);
        }

        return res;
    }
}
