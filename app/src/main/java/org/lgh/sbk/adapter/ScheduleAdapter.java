package org.lgh.sbk.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import org.lgh.sbk.R;
import org.lgh.sbk.entity.Schedule;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

public class ScheduleAdapter extends ArrayAdapter<Schedule> {

    private List<Schedule> schedules;

    public ScheduleAdapter(@NonNull Context context, @NonNull List<Schedule> scheduleList) {
        super(context, 0, scheduleList);
        schedules = new ArrayList<>();
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.row_schedule, parent, false
            );
        }
        TextView tvCourseName = convertView.findViewById(R.id.courseName);
        TextView tvRoom = convertView.findViewById(R.id.courseRoom);
        TextView tvTime = convertView.findViewById(R.id.courseTime);
        TextView tvAttendance = convertView.findViewById(R.id.attendence);
        TextView tvSlot = convertView.findViewById(R.id.txtSlot);


        Schedule schedule = getItem(position);

        if (schedule != null) {
            tvCourseName.setText(schedule.getCourseCode() + " - " + schedule.getCourseName());
            tvRoom.setText(schedule.getRoom());
            tvTime.setText( schedule.getStartTime() + " - " + schedule.getEndTime() );
            tvSlot.setText(schedule.getSlotName());
            tvAttendance.setText(schedule.getStatus());
//            tvAttendance.setTextColor();
        }

        return convertView;
    }
}
