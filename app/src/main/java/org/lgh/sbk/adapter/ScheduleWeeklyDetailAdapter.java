package org.lgh.sbk.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import org.lgh.sbk.R;
import org.lgh.sbk.entity.Schedule;

import java.util.ArrayList;
import java.util.List;

public class ScheduleWeeklyDetailAdapter extends ArrayAdapter<Schedule> {

    private List<Schedule> schedules;

    public ScheduleWeeklyDetailAdapter(@NonNull Context context, @NonNull List<Schedule> objects) {
        super(context, 0, objects);
        this.schedules = new ArrayList<>();
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.row_schedule_details, parent,false
            );
        }

        TextView courseNameDetail = convertView.findViewById(R.id.courseNameDetail);
        TextView instructorNameDetail = convertView.findViewById(R.id.instructorNameDetail);
        TextView courseRoomDetail = convertView.findViewById(R.id.courseRoomDetail);
        TextView slotDetail = convertView.findViewById(R.id.slotDetail);
        TextView courseTimeDetail = convertView.findViewById(R.id.courseTimeDetail);
        TextView attendenceDetail = convertView.findViewById(R.id.attendenceDetail);
        TextView groupStudentDetail = convertView.findViewById(R.id.groupStudentDetail);

        Schedule schedule = getItem(position);
        if (schedule != null) {
            courseNameDetail.setText(schedule.getCourseCode() + " - " + schedule.getCourseName());
            instructorNameDetail.setText(schedule.getInstructorName());
            courseRoomDetail.setText(schedule.getRoom());
            slotDetail.setText(schedule.getSlotName());
            courseTimeDetail.setText(schedule.getStartTime() + " - " + schedule.getEndTime());
            attendenceDetail.setText(schedule.getStatus());
            groupStudentDetail.setText(schedule.getGroupStudent());
        }

        return convertView;
    }
}
