package org.lgh.sbk.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import org.lgh.sbk.R;
import org.lgh.sbk.entity.ScheduleCustom;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.zip.Inflater;

public class ScheduleWeeklyAdapter extends ArrayAdapter<ScheduleCustom> {

    private List<ScheduleCustom> scheduleCustoms;
    private String dayOfMonthFormat = "";
    private String dayOfWeekFormat = "";
    public ScheduleWeeklyAdapter(@NonNull Context context, @NonNull List<ScheduleCustom> scheduleCustoms) {
        super(context, 0, scheduleCustoms);
        scheduleCustoms = new ArrayList<>();
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.item_day, parent, false
            );
        }

        TextView txtDayOfMonth = convertView.findViewById(R.id.txtDayOfMonth);
        TextView txtDayInWeek = convertView.findViewById(R.id.txtDayInWeek);
        TextView txtSubject = convertView.findViewById(R.id.txtSubject);

        ScheduleCustom scheduleCustom = getItem(position);

        try {
            dayOfMonthFormat = scheduleCustom.converDayOfMonth();
            dayOfWeekFormat = scheduleCustom.converDayOfWeek();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (scheduleCustom != null) {
            txtDayOfMonth.setText(dayOfMonthFormat);
            txtDayInWeek.setText(dayOfWeekFormat);
            txtSubject.setText(scheduleCustom.getScheduleList().stream()
                    .map(schedule -> schedule.getCourseName())
                    .collect(Collectors.joining("\n")));
        }

        return convertView;
    }
}
