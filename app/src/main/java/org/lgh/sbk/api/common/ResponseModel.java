package org.lgh.sbk.api.common;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ResponseModel implements Serializable {
    @SerializedName("msgId")
    private String msgId;
    @SerializedName("state")
    private String state;
    @SerializedName("data")
    @Expose
    private Object data;

    public ResponseModel() {
        this.msgId = "";
        this.state = "";
        this.data = null;
    }

    public ResponseModel(String msgId, String state, Object data) {
        this.msgId = msgId;
        this.state = state;
        this.data = data;
    }

    public String getMsgId() {
        return msgId;
    }

    public void setMsgId(String msgId) {
        this.msgId = msgId;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
