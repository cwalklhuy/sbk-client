package org.lgh.sbk.api;

import android.content.Intent;

import org.lgh.sbk.api.common.ResponseModel;
import org.lgh.sbk.entity.Account;
import org.lgh.sbk.entity.Course;
import org.lgh.sbk.entity.Professor;
import org.lgh.sbk.entity.RegisterSchedule;
import org.lgh.sbk.entity.Schedule;
import org.lgh.sbk.entity.Section;
import org.lgh.sbk.entity.Student;
import org.lgh.sbk.entity.json.SectionCreate;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface JsonPlaceHolderApi {

    @GET("/api/students")
    Call<List<Account>> getStudents();

    @GET("/student/login")
    Call<ResponseModel> getStudent(@Query("username") String username,
                                   @Query("password") String password);

    @POST("/student/create")
    Call<ResponseModel> addStudent(@Body Student student);

    @POST("/professor")
    Call<ResponseModel> addProfessor(@Body Professor professor);

    @POST("/course")
    Call<ResponseModel> addCourse(@Body Course course);


    //Tìm kiếm lịch học hôm nay
    @GET("/student/{studentId}")
    Call<List<Schedule>> getAllScheduleToday(@Path("studentId") Integer studentId);

    @GET("/student/{studentId}/{startDate}/{endDate}")
    Call<List<Schedule>> getAllScheduleRange(@Path("studentId") Integer studentId,
                                             @Path("startDate") String startDate,
                                             @Path("endDate") String endDate);

    @GET("/section")
    Call<List<Section>> getSections();

    @GET("/course")
    Call<List<Course>> getCourse();

    @GET("/professor")
    Call<List<Professor>> getProfessors();

    @POST("/schedule")
    Call<ResponseModel> makeSchedule(@Body RegisterSchedule registerSchedule);

    @POST("/section")
    Call<ResponseModel> makeSection(@Body SectionCreate section);
}
